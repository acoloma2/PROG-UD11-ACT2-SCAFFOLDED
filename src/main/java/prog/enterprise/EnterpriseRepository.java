package prog.enterprise;

import prog.exception.EnterpriseNotFoundException;

public interface EnterpriseRepository {

    /**
     *  Save Enterprise
     */
    public boolean save(Enterprise enterprise);

    /**
     * Find Enterprise with id EnterpriseId or return null
     */
     public Enterprise findById(int enterpriseId);

    /**
     * Find Enterprise by column name and value
     */
    public Enterprise findByNif(String nif);

    /**
     * Get Enterprise with id EnterpriseId or throws an EnterpriseNotFoundException
     */
     public Enterprise getById(int enterpriseId) throws EnterpriseNotFoundException;

}

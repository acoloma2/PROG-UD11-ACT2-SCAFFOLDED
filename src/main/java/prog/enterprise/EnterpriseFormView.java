package prog.enterprise;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.util.Duration;
import prog.TextFieldErrorLabel;
import prog.Validator.Validator;

import java.util.Locale;

public class EnterpriseFormView extends GridPane {

    private Label idLabel;
    private TextField idTextField;

    private Label nameLabel;
    private TextFieldErrorLabel nameTextField;

    private Label nifLabel;
    private TextFieldErrorLabel nifTextField;

    private Label addressLabel;
    private TextFieldErrorLabel addressTextField;

    private Label cityLabel;
    private TextFieldErrorLabel cityTextField;

    private Label provinceLabel;
    private TextFieldErrorLabel provinceTextField;

    private Label countryLabel;
    private ChoiceBox<String> countryChoiceBox;

    private Label localeLabel;
    private ChoiceBox<Locale> localeChoiceBox;

    private Label statusLabel;
    private CheckBox statusCheckbox;

    private EventHandler<MouseEvent> formEventHandler = new FormEnterpriseEventHandler();

    private Label infoLabel;
    private VBox infoPanel;

    private Validator validator;
    private EnterpriseRepository enterpriseRepository;

    public EnterpriseFormView(Validator validator, EnterpriseRepository enterpriseRepository){

        initView();
        initInfoPanel();
        this.validator = validator;
        this.enterpriseRepository = enterpriseRepository;

    }

    private void initView(){

        this.setAlignment(Pos.CENTER);
        this.setHgap(10);
        this.setVgap(5);
        this.setPadding(new Insets(15, 15, 30, 15));

        initInfoPanel();
        this.idLabel = new Label("Id:");
        this.idTextField = new TextField();
        this.idTextField.setEditable(false);
        this.add(idLabel, 0, 1);
        this.add(idTextField, 1, 1);

        this.nameLabel = new Label("Name:");
        this.nameTextField = new TextFieldErrorLabel();
        this.add(nameLabel, 0, 2);
        this.add(nameTextField, 1, 2);


        //TODO Añade los demás elementos del fomulario; nif, address, city, province, country


        Button saveButton = new Button("Save");
        this.add(saveButton, 0, 11, 2, 2);
        GridPane.setHalignment(saveButton, HPos.CENTER);
        saveButton.setId("save-bt");
        saveButton.addEventHandler(MouseEvent.MOUSE_CLICKED ,this.formEventHandler);
        countryChoiceBox.getSelectionModel().selectFirst();
        localeChoiceBox.getSelectionModel().selectLast();
    }

    private void initInfoPanel(){

        infoLabel = new Label("");
        Label titleInfoLabel = new Label("info:");
        infoPanel = new VBox(titleInfoLabel,infoLabel);
        infoPanel.getStyleClass().add("info-panel");
        infoPanel.setVisible(false);
        this.add(infoPanel, 0, 0, 2, 1);
        GridPane.setHalignment(infoPanel, HPos.CENTER);

    }

    private ObservableList<Locale> getValidLocales() {

        return FXCollections.observableArrayList(Locale.getAvailableLocales());

    }

    private ObservableList<String> getValidCountries() {

        return FXCollections.observableArrayList(Locale.getISOCountries());

    }

    private void submitForm(){

        if (validateForm()) {
            Enterprise enterprise = getEnterprise();
            //TODO Añade el código necesario para guardar la empresa
            //TODO una vez guardada correctamente llama al método showInfoMessage
            //TODO e indica que la empresa ha sido guarada con éxito
        }

    }

    private boolean validateForm() {

        boolean isValid = true;

        if (!this.validator.validateString(nameTextField.getText(), 1, 45)) {
            nameTextField.setError("* must be between 1 to 45 characters");
            isValid = false;
        } else {
            nameTextField.clear();
        }

        //TODO comprueba los demás campos del formulario que indicará si este es válido o no
        //TODO Deberás comprobar también que el nif no existe llamando a public Enterprise findByNif(String nif);

        return isValid;

    }

    private Enterprise getEnterprise() {

        Enterprise enterprise = null;

        //TODO crea y devuelve un objeto de tipo Empresa a partir de todos los datos introducidos en el Textfield
        //TODO return Enterprise enterprise = new Enterprise(
        //TODO        nameTextField.getText(),
        //TODO );

        return enterprise;

    }

    private void showInfoMessage(String message){

        this.infoPanel.setVisible(true);
        this.infoLabel.setText(message);
        Timeline fiveSecondsWonder = new Timeline(new KeyFrame(Duration.seconds(3), new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                infoPanel.setVisible(false);
            }
        }));
        fiveSecondsWonder.play();

    }

    class FormEnterpriseEventHandler implements EventHandler<MouseEvent>{
        @Override
        public void handle(MouseEvent event) {

            String controlId = ((Control) event.getSource()).getId();

            if (controlId.equals("save-bt")) {

                submitForm();

            }

        }
    }
}
